require streamdevice
require caenelsmagnetps,dev

## Set streamDevice search path
# caenelsmagnetps_TEMPLATES=/home/icssev/conda/envs/epicsdev/modules/caenelsmagnetps/dev/db

#epicsEnvSet ("STREAM_PROTOCOL_PATH", "$(caenelsmagnetps_TEMPLATES)")

## Database Macros
#epicsEnvSet("P","remote:")
#epicsEnvSet("R","fastps:")
#epicsEnvSet("P","LEBT-010:")
#epicsEnvSet("R","PwrC-PSCH-01:")
#epicsEnvSet("Amp2Tesla_calib","8.291E-4")


#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")


iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")


iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")



## Run IOC
iocInit()

