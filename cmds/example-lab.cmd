require streamdevice
require caenelsmagnetps,dev

## Set streamDevice search path
# caenelsmagnetps_TEMPLATES=/home/icssev/conda/envs/epicsdev/modules/caenelsmagnetps/dev/db

epicsEnvSet ("STREAM_PROTOCOL_PATH", "$(caenelsmagnetps_TEMPLATES)")

## Database Macros
#epicsEnvSet("P","remote:")
#epicsEnvSet("R","fastps:")
epicsEnvSet("P","LEBT-010:")
epicsEnvSet("R","PwrC-PSCH-01:")


## Device Specific
epicsEnvSet("FASTPS_IP","172.30.4.189")
epicsEnvSet("FASTPS_PORT","10001")

## Asyn port name
epicsEnvSet("ASYN_PORT", "FASTPS")

## Configure device
drvAsynIPPortConfigure("$(ASYN_PORT)","$(FASTPS_IP):$(FASTPS_PORT)",0,0,0)

## Load record instances
dbLoadRecords("caenels-fast-ps.template", "P=$(P),R=$(R),PORT=$(ASYN_PORT)")

## Run IOC
iocInit()

